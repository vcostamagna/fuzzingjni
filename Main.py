import os, sys
import threading

from core.analysis.ApkManager import ApkManager, AnalyzeApk
from core.analysis.FilterNativeOp import FilterNativeOp
from core.base.Task import Task
from core.threads.ThManager import ThManager
from core.parser import main as ilparser

import argparse

sys.path.insert(1, './androguard/')
static_cache_dir = "" #"/tmp/asd"
static_res_dir = "" #"/tmp/asd/results"
saaf_cmd = "MYPATH/scripts/run_saaf.sh -hl -nodb -c -k TARGET"
jaadas_cmd = \
    "java -jar MYPATH/jade-0.1.jar vulnanalysis -f TARGET -p /home/sid/android/android-sdk-linux/platforms" + \
    "-c /home/sid/JAADAS/jade/config/"

num_th = 1

def pushUpTo4(tasks_list, tm):
    global num_th
    res = []
    #num = 5
    if(len(tasks_list) < int(num_th)):
        num = len(tasks_list)
    for x in range(num_th):
        if len(tasks_list) <= 0: break;
        task = tasks_list.pop()
        res.append(task)
    tm.add_task(res)

def analyzeApks(dir):
    apk_manager = ApkManager(static_cache_dir, static_res_dir)
    apk_manager.addDir(dir.lstrip())
    tasks_list = []
    for target in apk_manager.get_apks_path():
        analysisTask = Task("analyze apk", AnalyzeApk(target, apk_manager))
        tasks_list.append(analysisTask)
    tm = ThManager(len(tasks_list))
    while True:
        if not tasks_list: break;
        print '------------------ spawning ANALYZERS threads -------- '
        pushUpTo4(tasks_list, tm)
        tm.wait_completition()
        print '------------------ END THREADS -------- '
    tasks_list = []
    for app in apk_manager.get_targets():
        op_filter_native = FilterNativeOp(app)
        task_native = Task("looking for JNI calls",op_filter_native)
        tasks_list.append(task_native)
    while True:
        if not tasks_list: break;
        print '------------------ spawning FILTERING threads -------- '
        pushUpTo4(tasks_list, tm)
        tm.wait_completition()
        print '------------------ END THREADS -------- '

    res = tm.get_res()
    if not os.path.exists(static_res_dir):
        os.makedirs(static_res_dir)
        os.chmod(static_res_dir,0755)
    fd = open(static_res_dir + "/" + "apks.json", "a")
    while True:
        if not res.empty():
            elem = res.get()
            name = elem.get_name()
            fd.write(name+".apk\n")
        else: break;
    fd.close()

def runParser(dir):
    print 'parsing in dir: ' + dir
    if not os.path.isdir(dir):
        return
    results = os.listdir(dir)
    tasks_list = []
    for root, dirs, files in os.walk(dir+"/"):
        #path = root.split('/')
        print("root ", os.path.basename(root))
        for file in files:
            if "entrypoints.jni" in file:
                path =  root + "/" + file
                if os.stat(path).st_size == 0: continue
                print "appending " + path
                op_parser = ilparser.parser(path)
                task_parser = Task("Parsing results", op_parser)
                tasks_list.append(task_parser)
    '''
    while True:
        if not results: break
        file = results.pop()
        print file
        continue
        op_parser = ilparser.parser(dir + "/" + file)
        task_parser = Task("Parsing results", op_parser)
        tasks_list.append(task_parser)
    '''
    tm = ThManager(len(tasks_list))
    while True:
        if not tasks_list: break;
        print '------------------ spawning PARSER threads -------- '
        pushUpTo4(tasks_list, tm)
        tm.wait_completition()
        print '------------------ END THREADS -------- '


def run_saaf(cmd):
    print 'running saaf... ' + cmd
    print 'mydir: ' + os.getcwd()
    os.system(cmd)


def start_saaf(saaf_path, dir):
    global saaf_cmd
    saaf_cmd = saaf_cmd.replace("MYPATH", saaf_path)
    for root, dirs, files in os.walk(dir+"/"):
        print("root ", os.path.basename(root))
        for file in files:
            if "apk.txt" in file:
                tmp_saaf_cmd = saaf_cmd.replace("TARGET", open(root+"/"+file, "r").readline().strip())
            if ".conf" in file:
                #run_saaf(root+"/"+file, saaf_path)
                cmd = "cp %s %s/conf/backtracking-patterns.xml" % (root+"/"+file, saaf_path)
                #print cmd
                os.system(cmd)
                t = threading.Thread(target=run_saaf, args=(tmp_saaf_cmd,))
                t.start()
                t.join()


def start_jaadas(jaadas_path, dir):
    global jaadas_cmd
    jaadas_cmd = jaadas_cmd.replace("MYPATH", jaadas_path)
    for root, dirs, files in os.walk(dir+"/"):
        print("root ", os.path.basename(root))
        for file in files:
            if "apk.txt" in file:
                tmp_cmd = jaadas_cmd.replace("TARGET", open(root+"/"+file, "r").readline().strip())
            if ".conf" in file:
                #run_saaf(root+"/"+file, saaf_path)
                cmd = "cp %s /home/sid/JAADAS/jade/config/SourcesAndSinks.txt" % (root+"/"+file)
                #print cmd
                os.system(cmd)
                t = threading.Thread(target=run_saaf, args=(tmp_cmd,))
                t.start()
                t.join()


def main(args):
    global static_cache_dir, static_res_dir
    '''
    if  ( args.parser and len(args.parser_dir) == 0):
        parser.print_help()
        return
    '''
    if( (len(args.target_dir) == 0
         and len(args.target_file) == 0
         and len(args.out_dir) == 0
         and len(args.saaf_dir) == 0
         and len(args.jaadas_dir) == 0) ):
        parser.print_help()
        return
    else:
        static_cache_dir = args.out_dir + "/cache"
        static_res_dir = args.out_dir + "/results"
        if len(args.target_dir) != 0:
            analyzeApks(args.target_dir)
        elif len(args.target_file) != 0:
            analyzeApks(args.target_file)
        elif len(args.jaadas_dir) != 0:
            if len(args.in_dir) == 0:
                parser.print_help()
                return
            runParser(args.in_dir)
            start_jaadas(args.jaadas_dir, args.in_dir)
        elif len(args.saaf_dir) != 0:
            if len(args.in_dir) == 0:
                parser.print_help()
                return
            runParser(args.in_dir)
            start_saaf(args.saaf_dir, args.in_dir)
        '''
        elif args.parser:
            if len(args.parser_dir) == 0:
                parser.print_help()
                return
            runParser(args.parser_dir)
        '''

if __name__ == "__main__":
    global num_th
    num_th = 2
    target_dir = target_file = target_jar = parser_dir = out_dir = saaf_dir = jaadas_dir = in_dir = ""
    parser = False
    parser = argparse.ArgumentParser(description='Processing apks.')
    parser.add_argument('-d', '--dir', action="store", dest="target_dir", default="",
                        help='apks dir', required=False)
    parser.add_argument('-f', '--apk', action="store", dest="target_file", default="",
                        help='apk file', required=False)
    parser.add_argument('-j', '--jar', type=str, action="store", dest="target_jar", default="",
                        help='jar file', required=False)
    parser.add_argument('-o', '--out-dir', action="store", dest="out_dir", default="",
                        help='out dir', required=False)
    '''
    parser.add_argument('-p', '--parser', action="store_true", dest="parser", default=False,
                        help='run parser', required=False)
    parser.add_argument('-pdir', '--pdir',type=str,  action="store", dest="parser_dir", default="",
                        help='parser dir', required=False)
    '''
    parser.add_argument('-saaf', '--saaf-path',type=str,  action="store", dest="saaf_dir", default="",
                        help='saaf path', required=False)
    parser.add_argument('-jaadas', '--jaadas-path',type=str,  action="store", dest="jaadas_dir", default="",
                        help='jaadas path', required=False)
    parser.add_argument('-indir', '--in-dir',type=str,  action="store", dest="in_dir", default="",
                        help='saaf in dir', required=False)
    parser.add_argument('-th', '--num-th',type=str,  action="store", dest="num_th", default="",
                        help='num threads', required=False)

    args = parser.parse_args()
    main(args)

