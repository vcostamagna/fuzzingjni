from core.base.Operation import Operation
from core.utils import *
from core.utils.writers import writeToSAAFTemplate
from core.utils.writers import writeToJAADASTemplate

import os

class parser(Operation):
    def __init__(self, f):
        self.dir = None
        self.file = f
        self.jni_entrypoints = []
        self.out_dir = ""
    def createDir(self,dir, name):
        dir = dir + name
        if not os.path.exists(dir):
            os.makedirs(dir)
            os.chmod(dir,0755)
        return dir
    def run(self,q):
        print 'running on: ' + self.file
        path_list = self.file.split('/')
        #print path_list
        s = ""
        for i in range(len(path_list)-1):
            s += path_list[i] + "/"
        print s
        #self.out_dir = self.create_saafDir(s, "saaf_conf")
        self.out_dir = self.createDir(s, "jaadas_conf")
        fp = open(self.file, "r")
        lines = fp.readlines()
        for l in lines:
            self.parse(l)
        self.filter()
    def parse(self, data):
        clsname = ""
        counter = 0
        for c in data:
            clsname += c
            counter += 1
            if c == ';':
                break
        remains = data[counter:]
        tmp = remains.split('(')
        mname = tmp.pop(0)
        clsname = clsname[1:-1]
        #print 'clsname: ' + clsname
        #print 'mname: ' + mname
        tmp = tmp.pop().split(')')
        args = tmp.pop(0).replace(" ","")
        returnvalue = tmp.pop()
        data = Data(clsname, mname, args,returnvalue)
        data._print()
        self.jni_entrypoints.append(data)
    def filter(self):
        print "----- filtering ----"
        f = Filter(self.jni_entrypoints, "I", self.out_dir)
        f.filter()
        print "----- end filtering ----"


class Filter:
    def __init__(self, datalist, pattern, out_dir):
        self.data = datalist
        self.pattern = pattern
        self.out_dir = out_dir
    def filter(self):
        writeToJAADASTemplate(self.data, self.out_dir)
    '''
    def filter(self):
        for elem in self.data:
            elem.arg_position()
            #print 'args1 ' + elem.get_args()
            pos = elem.get_first_interesting(self.pattern)
            if pos != -1:
                print 'trovato scrivo template'
                #writeToSAAFTemplate(elem, self.out_dir)
            writeToJAADASTemplate(elem, self.out_dir)
    '''

#Lpeer/ir/MainActivity;SendIR(Ljava/lang/String; I)I

class Data:
    def __init__(self,c, m ,a, ret ):
        self.args_type = ["I", "Z", "["]
        self.clsname = c
        self.mname = m
        self.args = a
        self.args_positions = {}
        self.ret = ret
    def get_first_interesting(self, pattern):
        for key, value in self.args_positions.iteritems():
            if pattern == value:
                #print 'trovato: ' +  str(key), str(value)
                return key
        return -1
    def get_ret(self):
        return self.ret
    def get_args(self):
        return self.args
    def get_mname(self):
        return self.mname
    def get_clsname(self):
        return self.clsname
    def arg_position(self):
        if len(self.args) == 0: return
        #print 'argomenti: ' + self.args
        tmp = self.args
        counter = 0
        i = 0
        while True:
            pos = tmp.find(";")
            if pos == -1:
                while i < len(tmp):
                    #print 'index : ' + str(i) + ' len: ' + str(len(tmp))
                    if tmp[i] == "[":
                        #array mode
                        c2 = tmp[i+1]
                        self.args_positions[counter] = tmp[i]+c2
                        i +=  2
                    else:
                        self.args_positions[counter] = tmp[i]
                        i += 1
                    #print self.args_positions
                    counter = counter + 1
                break
            self.args_positions[counter] = tmp[:pos]
            counter = counter + 1
            tmp = tmp[pos+1:]
        #print self.args_positions

    def _print(self):
        print "clsname: " + self.clsname
        print "mname: " + self.mname
        print "args: " + self.args


