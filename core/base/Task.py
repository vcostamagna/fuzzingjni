


class Task:
    def __init__(self, n, op):
        self.name = n
        self.operation = op
    def start(self, q):
        self.operation.run(q)
