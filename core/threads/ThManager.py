import threading
from Queue import Queue

class ThManager:
    def __init__(self,num_threads):
        #self.lock = threading.Lock()
        #self.num_threads = num_threads
        self.res_q = Queue(num_threads)
        self.threads = []
        #for _ in range(self.num_threads): Worker(self.tasks)
    def add_task(self, tasks_list):
        for t in tasks_list:
            #self.tasks.put(t)
            #Worker(self.tasks).start()
            th = threading.Thread(target=t.start, args=(self.res_q,))
            th.start()
            self.threads.append(th)

    def get_res(self):
        return self.res_q
    def wait_completition(self):
        for th in self.threads:
            th.join()
        #self.tasks.join()
        print "Thread Manager: tasks completed!"
    def areTaskRunning(self):
        print self.tasks.empty()
