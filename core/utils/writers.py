


orig_header = 	"""
        <?xml version="1.0" encoding="UTF-8" ?>
        <backtracking-patterns xmlns="http://syssec.rub.de/SAAF"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://syssec.rub.de/SAAF schema/backtracking-patterns.xsd">
        <backtracking-pattern active=\"true\" class=\"%s\"
	  description=\"reflection forname\" method=\"%s\" parameters=\"%s\"
      interesting=\"%s\" />
      </backtracking-patterns>""" % ("CLSNAME", "MNAME", "ARGS", "POS")

static_jaadasconf = \
"""
<android.content.Intent: android.os.Bundle getExtras()> -> _SOURCE_
<android.os.Bundle: java.lang.String getString(java.lang.String)> -> _SOURCE_
"""

jaadas_conf = \
"""
<%s: %s %s(%s)> -> _SINK_
""" % ("CLSNAME","RETTYPE", "MNAME", "ARGS")

#%<android.content.Context: void startActivity(android.content.Intent)> -> _SINK_


def writeToSAAFTemplate(data, outdir):
    global header
    pos = data.get_first_interesting("I");
    if pos == -1:
        print 'error'
        return
    writeSAAF(data.get_clsname(), data.get_mname(), data.get_args(), str(pos) )
    name = data.get_clsname().replace('/','.') + "_"+ data.get_mname() + ".conf";
    outdir += "/" + name
    print 'writing SAAF in: ' + outdir
    fp = open(outdir, "w")
    fp.write(header.strip())
    fp.close()
    print header


def transformSyntax(args):
    type = 'cippa'
    if(args == "I"):
        type = "int"
    elif(args == "Z"):
        type = "boolean"
    elif(args == "V"):
        type = "void"
    elif(args[0] == "L"):
        type = args[1:-1].replace('/','.')
    return type

def writeToJAADASTemplate(data, outdir):
    global header
    print 'write jaadas got dir: ' + outdir
    name = "jaadas.conf"
    outdir += "/" + name
    print 'writing JAADAS in: ' + outdir
    fp = open(outdir, "w")
    fp.write(static_jaadasconf + "\n")
    print 'DIO PORCO FESTOOOO'
    print data
    for elem in data:
        writeJAADAS(elem)
        #name = elem.get_clsname().replace('/','.') + "_"+ elem.get_mname() + ".conf";
        fp.write(header.strip())
        fp.write("\n")
        print header
    fp.close()


def writeJAADAS(data):
    global header
    clsname = data.get_clsname()
    mname = data.get_mname()
    args = data.get_args()
    ret = data.get_ret()
    #print '1return type: ' + ret.strip()
    #print '2arguments: ' + args
    type = transformSyntax(args.strip())
    ret2 = transformSyntax(ret.strip())
    #print '3return type: ' + ret2
    #print '4arguments: ' + type
    clsname = clsname.replace('/','.')
    #print
    header = jaadas_conf.replace("CLSNAME", clsname)
    header = header.replace("RETTYPE", ret2)
    header = header.replace("MNAME", mname)
    header = header.replace("ARGS", type)


def writeSAAF(clsname, mname, args, pos):
    global header
    header = orig_header.replace("CLSNAME", clsname)
    header = header.replace("MNAME", mname)
    header = header.replace("ARGS", args)
    header = header.replace("POS", pos)