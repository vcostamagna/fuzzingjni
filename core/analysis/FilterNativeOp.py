import sys
sys.path.insert(1, './androguard/')

from androguard.core.analysis import analysis

from core.base.Operation import Operation


class FilterNativeOp(Operation):
    def __init__(self,t):
        self.target = t
        self.result = None
    def get_result(self):
        return self.result

    def write_to_file(self,dataList):
        pkgname = self.target.get_name()
        filename = self.target.get_outdir() + "/" + "entrypoints.jni"
        fp = open(filename, "w")
        for data in dataList:
            fp.write(data)
            fp.write("\n")
        fp.close()

    def run(self, q):
        if self.checkNativeCode(self.target):
            self.result = self.target
            q.put(self.result)

    def checkNativeCode(self, t):
        print 'checking native code on : ' + t.get_name()
        vma = t.get_vma()
        self.write_to_file( analysis.get_NativeMethods(vma) )
        return analysis.is_native_code(vma)

    def checkReflectionCode(self,t):
        vma = t.get_vma()
        return analysis.is_reflection_code(vma)