import os,sys
from cPickle import dumps, loads

from core.base.Operation import Operation
import traceback
sys.path.insert(1, './androguard/')

from androguard.core.analysis import analysis
from androguard.core.bytecodes import apk
from androguard.core.bytecodes import dvm
from androguard.core import androconf

import hashlib

class ApkManager():
    def __init__(self, cache_dir=None, out_dir=None):
        self.targets = []
        self.apks_path = []
        self.cache_dir = cache_dir
        self.out_dir = out_dir

    def get_targets(self):
        return self.targets

    def get_apks_path(self):
        return self.apks_path

    def addApk(self, apk_path):
        self.apks_path.append(apk_path)

    def addDir(self, dir):
        if not os.path.isdir(dir):
            self.addApk(dir)
            return
        apks = os.listdir(dir)
        while True:
            if not apks: break
            a = apks.pop()
            self.addApk(dir + "/" + a)

class AnalyzeApk(Operation):
    def __init__(self, apk_path, manager):
        self.path = apk_path
        self.manager = manager
        self.cache_dir = self.manager.cache_dir
        self.out_dir = self.manager.out_dir

    def cache(self, target):
        #print 'saving session in ' + self.cache_dir
        if not os.path.exists(self.cache_dir):
            os.makedirs(self.cache_dir)
            os.chmod(self.cache_dir,0755)
        #print "name: " + target.get_name()
        l = [target.get_apk(), target.get_dvmf(), target.get_vma() ]
        with open(self.cache_dir + "/" + target.get_name(), "w") as fd:
            fd.write(dumps(l,-1))

    def checkCache(self, apk):
        #print "checking cache for: " + apk
        if not os.path.exists(self.cache_dir):
            return False
        apks = os.listdir(self.cache_dir)
        for a in apks:
            if a == apk:
                self.restore(apk)
                return True
        return False

    def restore(self, apk):
        path = self.cache_dir + "/" + apk
        print "restoring cache from: " + path
        a,d,dx = loads(open(path, "r").read())
        target = TargetApk(a,d,dx, path+".apk", self.compute_dex_hash(a.get_dex()), self.cache_dir, self.out_dir)
        self.manager.targets.append(target)

    def restoreAll(self):
        if not os.path.exists(self.cache_dir):
            return False
        apks = os.listdir(self.cache_dir)
        for apk in apks:
            self.restore(apk)
        return True

    def compute_dex_hash(self, bin):
        #print "computing hash: " + str(type(bin))
        return hashlib.sha1(bin).hexdigest()

    def create_outDir(self,hash, pkg):
        adir = self.out_dir + "/" + pkg + "_" + hash
        if not os.path.exists(adir):
            os.makedirs(adir)
            os.chmod(adir,0755)
        return adir
    def write_apkPath(self):
        fp = open(self.out_dir + "/" + "apk.txt", "w")
        fp.write(self.path)
        fp.close()

    def run(self,q):
        apk_path = self.path
        print "analyzing --> " + apk_path
        if self.checkCache(os.path.basename(os.path.splitext(apk_path)[0])):
            #print "found cache for: " + apk_path
            return True
        try:
            ret_type = androconf.is_android(apk_path)
            if ret_type == "APK":
                _a = apk.APK(apk_path)
                _dex = _a.get_dex()
                _dvmf = dvm.DalvikVMFormat(_dex)
            elif ret_type == "DEX":
                _a = None
                _dex = open(apk_path, "rb").read()
                _dvmf = dvm.DalvikVMFormat( _dex  )
            _vma = analysis.uVMAnalysis(_dvmf)
            #_newvma = analysis.newVMAnalysis(_dvmf)
            #_dvmf.set_vmanalysis(_vma)
            _hash = self.compute_dex_hash(_dex)
            if _a is None:
                _pkg = 'DUMMY'
            else:
                _pkg = _a.get_package()
            self.out_dir = self.create_outDir(_hash, _pkg )
            self.write_apkPath()
            target = TargetApk(_a, _dvmf, _vma, apk_path, _hash, self.cache_dir, self.out_dir)
            self.manager.targets.append(target)
            self.cache(target)
            if _pkg is None:
                print 'cannot retrive package name information for ' + apk_path
            return True
        except:
            print "cannot open the APK file", sys.exc_info()[0]
            traceback.print_exc(file=sys.stdout)
            return False


class TargetApk:
    def __init__(self,a,d,v,path, h, _dir, out_dir):
        self.apk = a
        self.dvmf = d
        self.vma = v
        self.path = path
        self.name = os.path.basename(os.path.splitext(self.path)[0])
        self.dex_hash = h
        self.cache_dir = _dir
        self.out_dir = out_dir
        print "target path: " + self.path + " target name: " + self.name
        print "hash: " + self.dex_hash
    def get_outdir(self):
        return self.out_dir
    def get_dir(self):
        return self.cache_dir
    def get_hash(self):
        return self.dex_hash
    def get_name(self):
        return self.name
    def get_vma(self):
        return self.vma
    def get_apk(self):
        return self.apk
    def get_dvmf(self):
        return self.dvmf

